package com.apirest.controllers;

import com.apirest.models.Producto;
import com.apirest.models.ProductoList;
import com.apirest.models.ProductoPrecioOnly;
import com.apirest.services.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("${url.base}")
public class ProductoController {

    @Autowired
    private ProductoService productService;

    @GetMapping("/productos")
    public ProductoList getProducts(){
        return this.productService.getProductos();
    }

    @GetMapping("/productos/{id}")
    public ResponseEntity getProductId(@PathVariable Long id){
        Producto producto = this.productService.getProducto(id);
        if (producto == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(producto);
    }

    @PostMapping("/productos")
    public ResponseEntity addProducto(@RequestBody Producto producto){
        this.productService.addProducto(producto);
        return new ResponseEntity<>("Product created sucessfully", HttpStatus.CREATED);
    }

    @PutMapping("/productos/{id}")
    public ResponseEntity updateProducto(@PathVariable int id, @RequestBody Producto productToUpdate){
        Producto pr = this.productService.getProducto((long) id);
        if(pr == null){
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        this.productService.updateProducto(id-1, productToUpdate);
        return  new ResponseEntity<>("Producto actualizado correctamente.", HttpStatus.OK);
    }

    @DeleteMapping("/productos/{id}")
    public ResponseEntity deleteProducto(@PathVariable int id){
        Producto pr = this.productService.getProducto((long)id);
        if(pr == null){
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        this.productService.removeProducto(id-1);
        return new ResponseEntity<>("Producto eliminado correctamente", HttpStatus.OK);
    }

    @PatchMapping("/productos/{id}")
    public ResponseEntity patchPrecioProducto(@PathVariable int id, @RequestBody ProductoPrecioOnly productoPrecioOnly){
        Producto pr = this.productService.getProducto((long) id);
        if(pr == null){
            return new ResponseEntity<>("Producto no econtrado", HttpStatus.NOT_FOUND);
        }
        pr.setPrecio(productoPrecioOnly.getPrecio());
        this.productService.updateProducto(id-1, pr);
        return new ResponseEntity<>("Producto actualizado correctamente.", HttpStatus.OK);
    }

    @GetMapping("/productos/{id}/users")
    public ResponseEntity getProductIdUsers(@PathVariable Long id){
        Producto pr = this.productService.getProducto(id);
        if(pr == null){
            return  new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
        }
        if(pr.getUsers() != null)
            return ResponseEntity.ok(pr.getUsers());
        else
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
