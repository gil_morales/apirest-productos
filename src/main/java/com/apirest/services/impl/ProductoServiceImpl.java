package com.apirest.services.impl;

import com.apirest.models.Producto;
import com.apirest.models.ProductoList;
import com.apirest.models.User;
import com.apirest.services.ProductoService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductoServiceImpl implements ProductoService {

    private ProductoList productos = new ProductoList();

    public ProductoServiceImpl(){
        productos.getProducts().add(new Producto(1L, "producto 1", 100.00));
        List<User> users = new ArrayList<>();
        users.add(new User("1"));
        users.add(new User("3"));
        users.add(new User("5"));
        productos.getProducts().get(0).setUsers(users);
    }

    @Override
    public ProductoList getProductos(){
        return productos;
    }

    @Override
    public Producto getProducto(Long id){
        for(Producto p : productos.getProducts()){
            if(p.getId().equals(id))
                return p;
        }
        return null;
    }

    @Override
    public Producto addProducto(Producto producto){
        productos.getProducts().add(producto);
        return  producto;
    }

    @Override
    public Producto updateProducto(int index, Producto newProduct) throws IndexOutOfBoundsException {
        productos.getProducts().set(index, newProduct);
        return  productos.getProducts().get(index);
    }

    @Override
    public void removeProducto(int index) throws IndexOutOfBoundsException {
        int pos = this.productos.getProducts().indexOf(this.productos.getProducts().get(index));
        this.productos.getProducts().remove(pos);
    }
}
