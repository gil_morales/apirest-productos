package com.apirest.services;

import com.apirest.models.Producto;
import com.apirest.models.ProductoList;
import org.springframework.http.ResponseEntity;

public interface ProductoService {

    public ProductoList getProductos();

    public Producto getProducto(Long id);

    public Producto addProducto(Producto producto);

    public Producto updateProducto(int index, Producto newProduct);

    public void removeProducto(int index);
}
