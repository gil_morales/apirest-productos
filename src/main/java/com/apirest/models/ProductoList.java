package com.apirest.models;

import java.util.ArrayList;
import java.util.List;

public class ProductoList {
    private List<Producto> products;

    public ProductoList() {
        products = new ArrayList<>();
    }

    public List<Producto> getProducts() {
        return products;
    }

    public void setProducts(List<Producto> products) {
        this.products = products;
    }
}
